# Adult / Kids Full Stack

# Description
This Stack will install 2x Sonarr, 2x Radarr, 2x Ombi

## Stack

#### Sonarr Adults
| Detail | Value |
| ------ | ------ |
| Domain | https://sonarr-adults.domain.com |

#### Sonarr Kids
| Detail | Value |
| ------ | ------ |
| Domain | https://sonarr-kids.domain.com |

#### Radarr Adults
| Detail | Value |
| ------ | ------ |
| Domain | https://radarr-adults.domain.com |

#### Radarr Kids
| Detail | Value |
| ------ | ------ |
| Domain | https://radarr-kids.domain.com |

#### Ombi Adults
| Detail | Value |
| ------ | ------ |
| Domain | https://ombi-adults.domain.com |

#### Ombi Kids
| Detail | Value |
| ------ | ------ |
| Domain | https://ombi-kids.domain.com |